FROM microsoft/dotnet

RUN apt -qqy update

RUN apt -qqy upgrade

RUN apt -qqy install curl git ssh zsh

# admin
RUN adduser --disabled-password --uid 1000 --geco '' --shell /bin/zsh app
ENV HOME=/home/app

USER root

WORKDIR $HOME

# COPY . $HOME

RUN chown -R app:app $HOME/

USER app

# FINIshin
CMD sleep infinity