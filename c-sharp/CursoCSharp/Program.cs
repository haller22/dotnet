﻿using System;
using System.Collections.Generic;

using CursoCSharp.Fundamentos;
using CursoCSharp.ClassesEMetodos;


namespace CursoCSharp {
    class Program {
        static void Main(string[] args) {
            var central = new CentralDeExercicios(new Dictionary<string, Action>() {
                // Fundamentos
                {"Primeiro Programa - Fundamentos", PrimeiroPrograma.Executar},
                {"Comentarios - Fundamentos", Comentarios.Executar},
                {"VariaveisEConstantes - Fundamentos", VariaveisEConstantes.Executar},
                {"Inferencia - Fundamentos", Inferencia.Executar},
                {"Interpolacao - Fundamentos", Interpolacao.Executar},
                {"NotacaoPonto - Fundamentos", NotacaoPonto.Executar},
                {"LendoDados - Fundamentos", LendoDados.Executar},
                {"FormatandoNumero - Fundamentos", FormatandoNumero.Executar},
                {"Conversoes - Fundamentos", Conversoes.Executar},

                // Classes E Metodos
                {"Menbros - ClassesEMetodos", Menbros.Executar},
                {"Construtores - ClassesEMetodos", Construtores.Executar},
                {"MetodosComRetorno - ClassesEMetodos", MetodosComRetorno.Executar},
                {"MetodosEstaticos - ClassesEMetodos", MetodosEstaticos.Executar},
                {"AtributosEstaticos - ClassesEMetodos", AtributosEstaticos.Executar},
                {"DesafioAtributo - ClassesEMetodos", DesafioAtributo.Executar},
                {"Params - ClassesEMetodos", Params.Executar},
                {"ParametrosNomeados - ClassesEMetodos", ParametrosNomeados.Executar},
                {"GetSet - ClassesEMetodos", GetSet.Executar},

            });

            central.SelecionarEExecutar();
        }
    }
}