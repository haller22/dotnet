namespace CursoCSharp.ClassesEMetodos
{
    public class ParametrosNomeados
    {
        public static void Formatar ( int dia, int mes, int ano ) {

	    System.Console.WriteLine("Data");
            System.Console.WriteLine($"{dia:D2}/{mes:D2}/{ano}");
        }
        public static void Executar (  ) {

            Formatar ( ano: 1999, dia: 12, mes: 5 );
        }
    }
}
