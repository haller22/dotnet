namespace CursoCSharp.ClassesEMetodos
{
    public class Menbros
    {

        public static void Executar (  ) {

            Pessoa sicrano = new Pessoa (  );

            sicrano.Nome = "Hall";
            sicrano.Idade = 22;

            System.Console.WriteLine($"Nome: {sicrano.Nome}, Idade: {sicrano.Idade}");

            sicrano.Zerar (  );

            System.Console.WriteLine($"Nome: {sicrano.Nome}, Idade: {sicrano.Idade}");
        }
    }
}