namespace CursoCSharp.ClassesEMetodos
{
    public class MetodosEstaticos
    {
        
        public class CalculadoraEstatica {

            public static int Multiplicar ( int num, int num1 ) {

                return num * num1;
            }
            public int Somar ( int num, int num1 ) {

                return num + num1;
            }
        }

        public static void Executar (  ) {

            var resultado = CalculadoraEstatica.Multiplicar ( 2,2 );
            System.Console.WriteLine($"Resultado Estatico {resultado}");

            var calc = new CalculadoraEstatica (  );

            System.Console.WriteLine($"Instancia {calc.Somar ( 2,2 )}");
            System.Console.WriteLine($"Estatico {CalculadoraEstatica.Multiplicar ( 2,2 )}");
        }
    }
}