namespace CursoCSharp.ClassesEMetodos
{

    public class Produtos {

        public string Nome;
        public double Preco;
        public static double Desconto = 0.1;

        public Produtos ( string nome, double preco ) {

            this.Nome = nome;
            this.Preco = preco;
        }

        public Produtos (  ) {

        }

        public double CalculaDesconto (  ) {

            return this.Preco - (this.Preco * Produtos.Desconto);
        }
    }
    public class AtributosEstaticos
    {
        public static void Executar (  ) {

            Produtos.Desconto = 0.3;

            var produto1 = new Produtos ( "Void", 30 );
            var produto2 = new Produtos ( "Void", 30 );

            System.Console.WriteLine($"Desconto {produto1.CalculaDesconto()}");
            System.Console.WriteLine($"Desconto {produto2.CalculaDesconto()}");
            
            Produtos.Desconto = 0.05;

            System.Console.WriteLine($"Desconto {produto1.CalculaDesconto()}");
            System.Console.WriteLine($"Desconto {produto2.CalculaDesconto()}");

        }
    }
}