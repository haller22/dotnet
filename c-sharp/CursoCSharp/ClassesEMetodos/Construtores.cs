namespace CursoCSharp.ClassesEMetodos
{
    class Carro {

        public string Modelo;
        public string Fabricante;
        public int Ano;

        public Carro (  ) {


        }
        
        // sobrecarga resolveu esse problema...
        //
        // public Carro ( string Modelo = "", string Fabricante = "", int Ano = 0 ) {
        public Carro ( string Modelo, string Fabricante, int Ano ) {

            this.Modelo = Modelo;
            this.Fabricante = Fabricante;
            this.Ano = Ano;
        }


    }

    public class Construtores
    {

        public static void Executar (  ) {

            var modelS = new Carro (  );

            modelS.Modelo = "S";
            modelS.Fabricante = "Tesla";
            modelS.Ano = 2009;

            System.Console.WriteLine(
                $"Model {modelS.Modelo} da {modelS.Fabricante} de {modelS.Ano}");
            var model3 = new Carro ( "3", "Tesla", 2016 );
            System.Console.WriteLine(
                $"Model {model3.Modelo} da {model3.Fabricante} de {model3.Ano}");

        }
    }
}