using System;

namespace CursoCSharp.ClassesEMetodos
{
    public class Moto {

        private string Modelo;
        private string Marca;
        private uint Cilindrada;

        public Moto ( string modelo, string marca, uint cilindrada ) {

            this.setModelo(modelo);
            this.setMarca(marca);
            this.setCilindrada(cilindrada);
        }

        public Moto (  ) {


        }

        public string getModelo (  ) {

            return this.Modelo;
        }
        public void setModelo ( string modelo ) {

            this.Modelo = modelo;
        }
        public string getMarca (  ) {

            return this.Marca;
        }
        public void setMarca ( string marca ) {

            this.Marca = marca;
        }
        public uint getCilindrada (  ) {

            return this.Cilindrada;
        }
        public void setCilindrada ( uint cilindrada ) {

            // this.Cilindrada = Math.Abs(cilindrada);
            this.Cilindrada = cilindrada;
        }
    }
    public class GetSet
    {
        public static void Executar (  ) {

            var moto = new Moto (  );

            moto.setMarca ( "Ronda" );
            moto.setModelo ( "Ronda" );
            moto.setCilindrada ( 0777 );

            var ronda = new Moto ( modelo: "Ronda", marca: "Ronda", cilindrada: 2222 );

            System.Console.WriteLine($"Marca: {moto.getMarca()}\nModelo: {moto.getModelo ( )}\nCilindrada: {moto.getCilindrada()}");
            System.Console.WriteLine($"Marca: {ronda.getMarca()}\nModelo: {ronda.getModelo ( )}\nCilindrada: {ronda.getCilindrada()}");

        }
    }
}