namespace CursoCSharp.ClassesEMetodos
{
    public class DesafioAtributo
    {
        int a = 10;
        public static void Executar (  ) {
            // Acesssar a variavel 'a'dentro do metodo Executar!
            var variavelA = (new DesafioAtributo()).a;

            System.Console.WriteLine(variavelA);

        }
    }
}