namespace CursoCSharp.ClassesEMetodos
{

    class CalculadoraComum {

        public double Somar ( double PrimeiroNumero, double SegundoNumero ) {
            
            return PrimeiroNumero + SegundoNumero;
        }
        public double Subtrair ( double PrimeiroNumero, double SegundoNumero ) {
            
            return PrimeiroNumero - SegundoNumero;
        }
        public double Dividir ( double PrimeiroNumero, double SegundoNumero ) {
            
            return PrimeiroNumero / SegundoNumero;
        }
        public double Multiplicar ( double PrimeiroNumero, double SegundoNumero ) {
            
            return PrimeiroNumero * SegundoNumero;
        }


    }

    class CalculadoraCadeia {

        double Memoria;

        public CalculadoraCadeia Somar ( double Numero ) {

            Memoria += Numero;
            return this;
        }
        public CalculadoraCadeia Subtrair ( double Numero ) {

            Memoria -= Numero;
            return this;
        }
        public CalculadoraCadeia Multiplicar ( double Numero ) {

            Memoria *= Numero;
            return this;
        }
        public CalculadoraCadeia Dividir ( double Numero ) {

            Memoria /= Numero;
            return this;
        }        
        public CalculadoraCadeia Zerar (  ) {

            Memoria = 0;
            return this;
        }
        public CalculadoraCadeia Imprimir (  ) {

            System.Console.WriteLine(Memoria);
            return this;
        }
        public double Resultado (  ) {

            return Memoria;
        }
    }
    public class MetodosComRetorno
    {

        public static void Executar (  ) {

            System.Console.WriteLine("Calculadora Comum: ");
            var calc1 = new CalculadoraComum (  );

            System.Console.WriteLine(calc1.Somar(3,2));
            System.Console.WriteLine(calc1.Subtrair(3,2));
            System.Console.WriteLine(calc1.Dividir(3,2));
            System.Console.WriteLine(calc1.Multiplicar(3,2));

            System.Console.WriteLine("Calculadora Cadeia: ");
            var calc2 = new CalculadoraCadeia (  );

            System.Console.WriteLine($"Soma de (3 + 2) * 2 = {calc2.Somar(3).Somar(2).Multiplicar(2).Resultado()}");
        }
    }
}