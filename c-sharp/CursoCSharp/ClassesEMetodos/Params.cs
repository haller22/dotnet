namespace CursoCSharp.ClassesEMetodos
{
    public class Params
    {

        public static void Apresentacao ( params string[] pessoas ) {

            foreach ( var pessoa in pessoas ) {

                System.Console.WriteLine($"Olá {pessoa}");
            }
        }
        public static void Executar (  ) {

            Apresentacao ( "Edson", "Gleidson", "Lucila", "Vinicios" );
        }
    }
}