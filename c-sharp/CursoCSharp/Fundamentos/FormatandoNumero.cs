using System;
using System.Globalization;

namespace CursoCSharp.Fundamentos
{
    public class FormatandoNumero
    {
        public static void Executar (  ) {

            double valor = 15.175;

            System.Console.WriteLine(valor.ToString("F1"));
            System.Console.WriteLine(valor.ToString("C"));
            System.Console.WriteLine(valor.ToString("P"));
            System.Console.WriteLine(valor.ToString("#.##"));
            
            CultureInfo cultura = new CultureInfo("pt-BR");
            System.Console.WriteLine(valor.ToString("C0", cultura));

            int acao = 256;
            System.Console.WriteLine(acao.ToString("D10", cultura));


        }
    }
}