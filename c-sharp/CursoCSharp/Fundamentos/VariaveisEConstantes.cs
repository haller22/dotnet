using System;

namespace CursoCSharp.Fundamentos
{
    public class VariaveisEConstantes
    {
        public static void Executar (  ) {

            // Area da Circunferencia

            double raio = 4.5;
            const double PI = 3.14;

            raio = 5.5;
            // PI = 3.1415;

            double area = PI * raio * raio;
            
            Console.WriteLine(area);
        
            // Tipos Inteiros

            bool estaChovendo = true;
            Console.WriteLine("Esta Chovendo " + estaChovendo);

            byte idade = 45;
            Console.WriteLine("Idade " + idade);
        
            sbyte saldoDeGols = sbyte.MinValue;
            Console.WriteLine("Valor Minimo sbyte "+ saldoDeGols);

            short salario = short.MinValue;
            Console.WriteLine("Valor short "+salario);

            int menorValorInt = int.MinValue;
            Console.WriteLine("Valor int" + menorValorInt);

            double universo = double.MinValue;
            Console.WriteLine("Valor double "+universo);

            long menorValorLong = long.MinValue;
            Console.WriteLine("Menor Valor long "+menorValorLong);

            decimal distanciaEntreEstrelas = 1_000_000_000_000;
            System.Console.WriteLine("Distancia Entre Estrelas "+distanciaEntreEstrelas);

            char letra = 'd';
            System.Console.WriteLine("Letra "+letra);

            string palavra = "Deus eh Maior";
            System.Console.WriteLine("Palavra "+palavra);
        }
    }
}