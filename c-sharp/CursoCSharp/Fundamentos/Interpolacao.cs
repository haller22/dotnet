using System;

namespace CursoCSharp.Fundamentos
{
    public class Interpolacao
    {
        
        public static void Executar (  ) {

            string nome = "notebook gamer";
            string marca = "Dell";
            double preco = 5800.00;

            System.Console.WriteLine("O "+nome+" da marca "+
                        marca+" Custa "+preco);
            System.Console.WriteLine("O {0} da marca {1} custa {2}.",
                        nome,marca,preco);
            System.Console.WriteLine($"A marca {marca} eh legal");
            System.Console.WriteLine($"1 + 1 = { 1 + 1 }");
        }
    }
}