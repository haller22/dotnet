using System;

namespace CursoCSharp.Fundamentos
{
    public class Inferencia
    {
        public static void Executar (  ) {

            var nome = "Leonardo";
            System.Console.WriteLine(nome);
        }
    }
}