using System;
using System.Globalization;

namespace CursoCSharp.Fundamentos
{
    public class LendoDados
    {
        public static void Executar (  ) {

            System.Console.Write("Digite seu nome: ");
            string nome = System.Console.ReadLine (  );
            System.Console.Write("Qual e sua idade: ");
            int idade = int.Parse ( System.Console.ReadLine (  ) );
            System.Console.Write("Qual e seu salario: ");
            double salario = double.Parse ( System.Console.ReadLine (  ),
                                                CultureInfo.InvariantCulture );
            // System.Console.WriteLine(nome);
            // System.Console.WriteLine(idade);
            // System.Console.WriteLine(salario);

            System.Console.WriteLine($"{nome} {idade} R$ {salario}");
        }
    }
}