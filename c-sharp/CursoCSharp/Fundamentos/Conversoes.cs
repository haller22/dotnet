using System;

namespace CursoCSharp.Fundamentos
{
    public class Conversoes
    {
        public static void Executar (  ) {

            int inteiro = 11;
            double quebrado = inteiro;

            System.Console.WriteLine($"{inteiro} => {quebrado}");

            double quebrado_casting = 11;
            int inteiro_casting = (int) quebrado_casting;

            System.Console.WriteLine($"{quebrado_casting} => {inteiro_casting}");

            System.Console.Write("Digite sua Idade : ");
            int idade = int.Parse(System.Console.ReadLine());
            System.Console.WriteLine($"Sua Idade {idade}");

            System.Console.Write("Digite sua Idade : ");
            int.TryParse(System.Console.ReadLine(), out int idade2);
            System.Console.WriteLine($"Sua Novamente Idade {idade2}");

        }
    }
}